// SPDX-License-Identifier: BSD-3-Clause
package org.islandoftex.texdoconline

import io.ktor.application.*
import io.ktor.features.*
import io.ktor.http.*
import io.ktor.http.content.*
import io.ktor.jackson.*
import io.ktor.response.*
import io.ktor.routing.*
import io.ktor.util.*
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.withContext
import org.islandoftex.ctanapi.CTAN
import org.islandoftex.texdoc.TeXDoc
import org.islandoftex.texdoconline.html.Pages
import org.islandoftex.texdoconline.model.InternalCTANTopic
import org.islandoftex.texdoconline.model.InternalTeXdocEntry
import org.islandoftex.texdoconline.model.VersionInformation
import java.io.File
import java.io.IOException
import java.text.SimpleDateFormat

/**
 * The webserver's main entry point and routing table.
 */
@KtorExperimentalAPI
@Suppress("LongMethod")
fun Application.module() {
    install(DefaultHeaders)
    install(CallLogging)
    install(ContentNegotiation) {
        jackson { }
    }
    install(StatusPages) {
        status(HttpStatusCode.NotFound) {
            call.respondRedirect("/missing.html")
        }
    }
    install(Routing) {
        get("/") {
            call.respondRedirect("/index.html", true)
        }
        get("/pkg/{name}") {
            // consistency path for compatibility with the current texdoc.net
            // this serves the entry texdoc would open on a local installation;
            // this is equivalent to serving the first (0th) entry of the /texdoc
            // API path
            try {
                call.respondRedirect("/serve/${call.parameters["name"]!!}/0", permanent = true)
            } catch (_: KotlinNullPointerException) {
                call.respond(HttpStatusCode.UnprocessableEntity, "No package given")
            }
        }
        get("/texdoc/{package}") {
            // respond with all documentation entries texdoc is able to find
            // for the given package name
            try {
                val pkgName = call.parameters["package"]!!
                call.respond(withContext(Dispatchers.IO) {
                    TeXDoc.getEntries(pkgName).sortedByDescending { it.score }.map {
                        InternalTeXdocEntry(
                            path = it.path.toAbsolutePath().toString().removePrefix("${Session.texmfDist}/doc"),
                            description = it.description
                        )
                    }
                })
            } catch (_: KotlinNullPointerException) {
                call.respond(HttpStatusCode.UnprocessableEntity, "No package given")
            }
        }
        get("/topics/list") {
            // list all CTAN topics
            call.respond(
                Session.cache.getAllOrElse {
                    CTAN.topics.map { it.name to it }.toMap()
                }.map {
                    InternalCTANTopic(it.value.name, it.value.details)
                }
            )
        }
        get("/topic/{topic}") {
            // list all packages belonging to a topic
            try {
                val topicName = call.parameters["topic"]!!
                call.respond(
                    Session.cache.getOrElse(topicName) {
                        CTAN.topics.map { it.name to it }.toMap()
                    }.let {
                        InternalCTANTopic(
                            key = it.name,
                            details = it.details,
                            packages = it.packages
                        )
                    }
                )
            } catch (_: KotlinNullPointerException) {
                call.respond(HttpStatusCode.UnprocessableEntity, "No topic given")
            } catch (_: NoSuchElementException) {
                call.respond(HttpStatusCode.UnprocessableEntity, "Topic not found")
            }
        }
        get("/serve/{package}/{id}") {
            // serve the documentation file corresponding to entry id
            // in the list of documentation files for package package
            try {
                val pkgName = call.parameters["package"]!!
                val pkgId = call.parameters["id"]!!.toInt()
                call.respondFile(withContext(Dispatchers.IO) {
                    TeXDoc.getEntries(pkgName)[pkgId].path.toFile()
                })
            } catch (_: KotlinNullPointerException) {
                call.respond(HttpStatusCode.UnprocessableEntity, "No package to serve")
            } catch (_: NumberFormatException) {
                call.respond(HttpStatusCode.UnprocessableEntity, "Expecting a valid index")
            } catch (_: IndexOutOfBoundsException) {
                call.respond(HttpStatusCode.UnprocessableEntity, "Index out of bounds")
            }
        }
        get("/version") {
            try {
                call.respond(
                    VersionInformation(
                        api = Session.API_VERSION,
                        texdoc = TeXDoc.version,
                        tlpdb = withContext(Dispatchers.IO) {
                            SimpleDateFormat("yyyy-MM-dd'T'HH:mm:ssXXX").format(
                                File(Session.texmfDist).parentFile.resolve("tlpkg/texlive.tlpdb").lastModified()
                            )
                        }
                    )
                )
            } catch (_: IOException) {
                call.respond(VersionInformation(Session.API_VERSION, TeXDoc.version, "UNKNOWN"))
            }
        }
        get("/missing.html") {
            Session.resourceDirectory
                ?.resolve("missing.html")
                ?.takeIf { it.exists() }
                ?.let {
                    call.respondFile(it)
                }
                ?: call.respondText(Pages.missingPage, ContentType.Text.Html)
        }
        get("/index.html") {
            Session.resourceDirectory
                ?.resolve("index.html")
                ?.takeIf { it.exists() }
                ?.let {
                    call.respondFile(it)
                }
                ?: call.respondText(Pages.mainPage, ContentType.Text.Html)
        }
        static("/") {
            Session.resourceDirectory?.let {
                filesWithResourceFallback(it, "org/islandoftex/texdoconline")
            } ?: resources("org/islandoftex/texdoconline")
        }
    }
}
