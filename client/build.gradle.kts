// SPDX-License-Identifier: BSD-3-Clause

import org.islandoftex.texdoconline.build.Versions

plugins {
    kotlin("js")
}

kotlin {
    js {
        moduleName = "texdoc-online"

        useCommonJs()
        browser {
        }
        binaries.executable()
    }
}

dependencies {
    implementation(project(":shared"))
    implementation(kotlin("stdlib-js", Versions.kotlin))
    implementation("io.ktor:ktor-client-js:${Versions.ktor}")
    implementation("io.ktor:ktor-client-serialization:${Versions.ktor}")
    implementation("org.jetbrains.kotlinx:kotlinx-serialization-core-js:${Versions.serialization}")
    implementation("org.jetbrains.kotlinx:kotlinx-serialization-json-js:${Versions.serialization}")
    implementation("org.jetbrains.kotlinx:kotlinx-html-js:${Versions.html}")
    implementation("org.jetbrains.kotlinx:kotlinx-coroutines-core-js:${Versions.coroutines}")
}
